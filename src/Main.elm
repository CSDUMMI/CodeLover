module Main exposing (..)

import Html.Events
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Browser
import Dict exposing (Dict)
import Set exposing (Set)
import Json.Decode as D
import Json.Encode as E
import Url.Builder as Builder
import Http
import Time
import Progression exposing (Progression(..))

main = Browser.document
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- TYPES
type alias URL = String

type alias MeetingURL = URL

type alias CodebergUsername = String

type alias Suggestion
    = { viewed : Bool
      , description : String
      , title : String
      , date : Time.Posix
      , comments : Int
      , link : URL
      , sender : CodebergUsername
      }

type alias AcceptedSuggestion
    = { viewed : Bool
      , title : String
      , receiver : CodebergUsername
      , link : URL
      }

type alias Match
    = { viewed : Bool
      , sender : CodebergUsername
      , receiver : CodebergUsername
      , issue : URL
      , meeting : MeetingURL
      }

-- DECODERS
matchDecoder : D.Decoder Match
matchDecoder =
    D.map4 (Match False)
        (D.field "sender" D.string)
        (D.field "receiver" D.string)
        (D.field "issue" D.string)
        (D.field "meetingURL" D.string)

suggestionDecoder : D.Decoder Suggestion
suggestionDecoder =
    D.map6 (Suggestion False)
        (D.field "description" D.string)
        (D.field "title" D.string)
        (D.map Time.millisToPosix (D.field "date" D.int))
        (D.field "comments" D.int)
        (D.field "link" D.string)
        (D.field "sender" D.string)

acceptedSuggestionDecoder : D.Decoder AcceptedSuggestion
acceptedSuggestionDecoder =
    D.map3 (AcceptedSuggestion False)
        (D.field "title" D.string)
        (D.field "receiver" D.string)
        (D.field "link" D.string)

-- MODEL
type alias Model
    = { codebergUsername : Maybe CodebergUsername
      , usernameEntered : Bool
      , newSearch : Maybe URL
      , searches : Dict URL Bool
      , suggestions : Dict URL Suggestion
      , acceptedSuggestions : Dict URL AcceptedSuggestion
      , matches : Dict URL Match
      , error : Maybe Http.Error
      }

popDict : Dict comparable v -> Maybe (comparable, v)
popDict d = case List.head <| Dict.keys d of
    Just k -> case Dict.get k d of
        Just v -> Just (k,v)
        Nothing -> Nothing
    Nothing -> Nothing

init : () -> (Model, Cmd Msg)
init () =
        ({ codebergUsername = Nothing
        , usernameEntered = False
        , newSearch = Nothing
        , searches = Dict.empty
        , matches = Dict.empty
        , suggestions = Dict.empty
        , acceptedSuggestions = Dict.empty
        , error = Nothing
        }, Cmd.none
        )

-- UPDATE
type Msg
    = Accept URL
    | Reject URL
    | FetchSuggestions
    | SuggestionsReceived (Result Http.Error (Dict URL Suggestion))
    | FetchAcceptedSuggestions
    | AcceptedSuggestionsReceived (Result Http.Error (Dict URL AcceptedSuggestion))
    | FetchMatches
    | MatchesReceived (Result Http.Error (Dict URL Match))
    | RequestSent (Result Http.Error Bool)
    | StartNewSearch
    | ChangeIssueURL URL
    | EnterIssueURL
    | ChangeUsername CodebergUsername
    | EnterUsername
    | ResponseToAcceptedSuggestion URL Bool
    | NoOp

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of

        NoOp -> (model, Cmd.none)

        Accept url -> ({ model
                       | suggestions = Dict.update url (Maybe.map (\s -> { s | viewed = True })) model.suggestions
                       }
                       , acceptCmd model url)

        Reject url -> ({ model
                       | suggestions = Dict.update url (Maybe.map (\s -> { s | viewed = True })) model.suggestions
                       }
                       , rejectCmd model url)

        ResponseToAcceptedSuggestion url response -> ({ model
                                 | acceptedSuggestions = Dict.update url (Maybe.map (\s -> { s | viewed = True })) model.acceptedSuggestions
                                 }, if response then acceptCmd model url else rejectCmd model url )

        RequestSent result ->
            case result of
                Ok _ -> (model, Cmd.none)
                Err error -> ({ model | error = Just error }, Cmd.none)

        FetchSuggestions -> (model, fetchSuggestions model)

        SuggestionsReceived result ->
            case result of
                Ok suggestions -> ({ model | suggestions = Dict.union model.suggestions suggestions }, Cmd.none)
                Err error -> ({ model | error = Just error }, Cmd.none)

        FetchAcceptedSuggestions -> (model, fetchAcceptedSuggestions model)

        AcceptedSuggestionsReceived result ->
            case result of
                Ok suggestions -> ({ model | acceptedSuggestions = Dict.union model.acceptedSuggestions suggestions }, Cmd.none)
                Err error -> ({ model | error = Just error }, Cmd.none)

        FetchMatches -> (model, fetchMatches model)

        MatchesReceived result ->
            case result of
                Ok matches -> ({ model | matches = Dict.union model.matches matches }, Cmd.none)
                Err error -> ({ model | error = Just error }, Cmd.none)

        ChangeUsername username ->
            ({ model | codebergUsername = Just username, usernameEntered = False }
            , Cmd.none
            )
        EnterUsername -> ({ model | usernameEntered = True }, Cmd.none)
        StartNewSearch ->
            ({ model | newSearch = Just "" }
            , Cmd.none
            )

        ChangeIssueURL url ->
            (case model.newSearch of
                Just _ -> { model | newSearch = Just url }
                Nothing -> model
            , Cmd.none
            )

        EnterIssueURL ->
            case model.newSearch of
                Just url ->
                    ( { model | newSearch = Nothing
                              , searches = Dict.insert url True model.searches
                              }
                    , registerSearching (Maybe.withDefault "" model.codebergUsername) url
                    )
                Nothing -> (model, Cmd.none)

-- CMDS
fetchSuggestions : Model -> Cmd Msg
fetchSuggestions model =
    Http.get
        { url = Builder.absolute [ "suggestions" ] [ Builder.string "codebergUsername" <| Maybe.withDefault "" model.codebergUsername ]
        , expect = Http.expectJson SuggestionsReceived <| D.dict suggestionDecoder
        }

fetchAcceptedSuggestions : Model -> Cmd Msg
fetchAcceptedSuggestions model =
    Http.get
        { url = Builder.absolute [ "accepted_suggestions" ] [ Builder.string "codebergUsername" <| Maybe.withDefault "" model.codebergUsername ]
        , expect = Http.expectJson AcceptedSuggestionsReceived <| D.dict acceptedSuggestionDecoder
        }

fetchMatches : Model -> Cmd Msg
fetchMatches model =
    Http.get
        { url = Builder.absolute [ "matches" ] [ Builder.string "codebergUsername" <| Maybe.withDefault "" model.codebergUsername ]
        , expect = Http.expectJson MatchesReceived <| D.dict matchDecoder
        }

registerSearching : CodebergUsername -> URL -> Cmd Msg
registerSearching codebergUsername url =
    Http.get
        { url = Builder.absolute [ "searching" ] [ Builder.string "issue" url, Builder.string "codebergUsername" codebergUsername ]
        , expect = Http.expectJson RequestSent D.bool
        }

acceptCmd : Model -> URL -> Cmd Msg
acceptCmd model url =
    Http.get
        { url = Builder.absolute [ "accept" ] [ Builder.string "issue" url, Builder.string "codebergUsername" <| Maybe.withDefault "" model.codebergUsername ]
        , expect = Http.expectJson RequestSent D.bool
        }

rejectCmd : Model -> URL -> Cmd Msg
rejectCmd model url =
    Http.get
        { url = Builder.absolute [ "reject" ] [ Builder.string "issue" url,  Builder.string "codebergUsername" <| Maybe.withDefault "" model.codebergUsername ]
        , expect = Http.expectJson RequestSent D.bool
        }

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model = Sub.batch
    [ Time.every 1000 (\_ -> if model.usernameEntered && (Dict.isEmpty <| Dict.filter (\k v -> not v.viewed) model.matches)
            then FetchMatches
            else NoOp)
    , Time.every 1000 (\_ -> if model.usernameEntered && (Dict.isEmpty <| Dict.filter (\k v -> not v.viewed) model.acceptedSuggestions)
            then FetchAcceptedSuggestions
            else NoOp)
    , Time.every 1000 (\_ -> if model.usernameEntered && (Dict.isEmpty <| Dict.filter (\k v -> not v.viewed) model.suggestions)
        then FetchSuggestions
        else NoOp)
    ]

-- VIEW
view : Model -> Browser.Document Msg
view model =
    { title = "CodeLovers"
    , body =
        [ Element.layout [] <| Element.column []
          [ viewUserLogin model.codebergUsername
          , viewSearch model.newSearch
          , let unviewedExists = not << Dict.isEmpty << Dict.filter (\k v -> not v.viewed)
                getUnviewed = popDict << Dict.filter (\k v -> not v.viewed)
            in if unviewedExists model.matches
                then viewFoundMatch << Maybe.map Tuple.second <| getUnviewed model.matches
                else if unviewedExists model.acceptedSuggestions
                    then viewAcceptedSuggestion << Maybe.map Tuple.second <| getUnviewed model.acceptedSuggestions
                    else if unviewedExists model.suggestions
                        then viewSuggestion << Maybe.map Tuple.second <| getUnviewed model.suggestions
                        else Element.text "No suggestions or matches found"
          , Element.text <|
            case model.error of
                Just error -> case error of
                    Http.BadUrl url -> "Bad URL Error " ++ url
                    Http.Timeout -> "Timeout Error"
                    Http.NetworkError -> "Network Error"
                    Http.BadStatus i -> "Bad Status " ++ String.fromInt i
                    Http.BadBody s -> "Bad Body " ++ s
                Nothing -> ""
          , Element.text <| String.fromInt (Dict.size <| Dict.filter (\k v -> v.viewed) model.suggestions) ++ "/" ++ String.fromInt (Dict.size model.suggestions) ++ " suggestions viewed"
          , Element.text << String.fromInt << Dict.size <| Dict.filter (\k v -> v.viewed) model.suggestions
          ]
        ]
    }

viewUserLogin : Maybe CodebergUsername -> Element Msg
viewUserLogin codebergUsername =
    Input.text [onEnter EnterUsername]
        { text = Maybe.withDefault "" codebergUsername
        , onChange = ChangeUsername
        , placeholder = Just << Input.placeholder [] <| Element.text "Username"
        , label = Input.labelAbove [] <| Element.text "Username"
        }

viewSearch : Maybe URL -> Element Msg
viewSearch newSearch =
    case newSearch of
                Nothing -> Input.button []
                    { onPress = Just StartNewSearch
                    , label = Element.text "New Search"
                    }
                Just url -> Input.text [onEnter EnterIssueURL]
                    { onChange = ChangeIssueURL
                    , text = url
                    , placeholder = Just << Input.placeholder [] <| Element.text "https://codeberg.org"
                    , label = Input.labelAbove [] <| Element.text "Search"
                    }

viewFoundMatch : Maybe Match -> Element Msg
viewFoundMatch
    = Maybe.withDefault Element.none << Maybe.map (\match ->
        Element.column [Element.spaceEvenly]
            [ Element.el [Font.size 24] <| Element.text "Match"
            , Element.newTabLink []
                { url = "https://codeberg.org/" ++ match.sender
                , label = Element.text <| "@" ++ match.sender
                }
            , Element.newTabLink []
                { url = "https://codeberg.org/" ++ match.receiver
                , label = Element.text <| "@" ++ match.receiver
                }
            , Element.newTabLink []
                { url = match.meeting
                , label = Element.text <| "Join meeting"
                }
            , Element.newTabLink []
                { url = match.issue
                , label = Element.text "View the issue"
                }
            ]
    )

viewAcceptedSuggestion : Maybe AcceptedSuggestion -> Element Msg
viewAcceptedSuggestion
    = Maybe.withDefault Element.none << Maybe.map(\acceptedSuggestion ->
        Element.column [Element.spaceEvenly]
            [ Element.el [Font.size 24] <| Element.text "Somebody accepted your search"
            , Element.newTabLink [Font.size 18]
                { label = Element.text acceptedSuggestion.title
                , url = acceptedSuggestion.link
                }
            , Element.newTabLink []
                { label = Element.text <| "Accepted by: @" ++ acceptedSuggestion.receiver
                , url = "https://codeberg.org/" ++ acceptedSuggestion.receiver
                }
            , Input.button []
                { onPress = Just <| ResponseToAcceptedSuggestion acceptedSuggestion.link True
                , label = Element.text "Accept"
                }
            , Input.button []
                { onPress = Just <| ResponseToAcceptedSuggestion acceptedSuggestion.link False
                , label = Element.text "Reject"
                }
            ])

viewSuggestion : Maybe Suggestion -> Element Msg
viewSuggestion
    = Maybe.withDefault Element.none << Maybe.map(\suggestion ->
        Element.column [Element.spaceEvenly]
            [ Element.el [Font.size 24] <| Element.text "Suggestion"
            , Element.newTabLink [Font.size 18]
                { label = Element.text suggestion.title
                , url = suggestion.link
                }
            , Element.paragraph []
                [ Element.text suggestion.description
                , Element.text <| "Comments: " ++ String.fromInt suggestion.comments
                ]
            , Element.newTabLink []
                    { label = Element.text <| "@" ++ suggestion.sender
                    , url = "https://codeberg.org/" ++ suggestion.sender
                    }
            , Element.row []
                [Input.button [Font.color <| Element.rgb255 0 255 0]
                    { onPress = Just <| Accept suggestion.link
                    , label = Element.text "Accept"
                    }
                , Input.button [Font.color <| Element.rgb255 255 0 0]
                    { onPress = Just <| Reject suggestion.link
                    , label = Element.text "Reject"
                    }
                ]
            ])

onEnter : msg -> Element.Attribute msg
onEnter msg =
    Element.htmlAttribute
        (Html.Events.on "keyup"
            (D.field "key" D.string
                |> D.andThen
                    (\key ->
                        if key == "Enter" then
                            D.succeed msg

                        else
                            D.fail "Not the enter key"
                    )
            )
        )

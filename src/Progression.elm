module Progression exposing ( Progression(..)
                            , withDefault
                            , isFinished
                            )

type Progression a b
    = NotStarted
    | InProgress a
    | Finished b

withDefault : b -> Progression a b -> b
withDefault def progression =
    case progression of
        Finished d -> d
        _ -> def

isFinished : Progression a b -> Bool
isFinished progression =
    case progression of
       Finished _ -> True
       _ -> False

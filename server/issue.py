from urllib.parse import urlparse
import requests

class InvalidUserInput(Exception):
    pass

def parse_issue_url(html_url):
    try:
        path = urlparse(html_url).path.split("/")

        owner = path[1]

        repo = path[2]

        id = int(path[4])

        return (owner, repo, id)
    except IndexError:

        raise InvalidUserInput(html_url)

def get_issue(html_url, gitea_instance):
    owner, repo, id = parse_issue_url(html_url)

    url = f"{gitea_instance}/api/v1/repos/{owner}/{repo}/issues/{id}"

    response = requests.get(url)

    if response.ok:
        return response.json()

    return False




import random
import os

WORDS = open(os.environ.get("WORDS", "../words.txt")).read().split("\n")

def get_random_words(k = 5):

    return "".join(map(lambda s: s.capitalize(), random.sample(WORDS, k)))

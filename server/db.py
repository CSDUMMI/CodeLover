import peewee as pw
import os
import datetime

dbname = os.environ.get("CODE_LOVER_DB_NAME", "codelover.db")

db = pw.SqliteDatabase(dbname)

class BaseModel(pw.Model):
    class Meta:
        database = db

class User(BaseModel):
    username = pw.TextField(primary_key=True)

class Search(BaseModel):
    url = pw.TextField()

    searcher = pw.ForeignKeyField(User, backref="searches")

    expires = pw.DateTimeField()

class Suggestion(BaseModel):
    search = pw.ForeignKeyField(Search, backref="suggestions")

    receiver = pw.ForeignKeyField(User, backref="suggestions_received")

    accepted_by_receiver = pw.BooleanField(null=True)

    accepted_by_sender = pw.BooleanField(null=True)

    meeting = pw.TextField(null=True)

db.create_tables([
    User,
    Suggestion,
    Search
])

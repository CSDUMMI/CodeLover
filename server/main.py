"""
API Implementation for the client implemented in src/:

- /suggestions?codebergUsername=<username> returns new suggestions for the user
- /matches?codebergUsername=<username> returns the new matches a user has
- /accept?issue=<issue>&codebergUsername=<username> sets the suggestions at issue to be accepted.
- /reject?issue=<issue>&codebergUsername=<username> sets the suggestion at the issue to be rejected.
- /searching?issue=<issue>&codebergUsername=<username> registers the user to be searching for a match on the issue.
"""
from flask import Flask, request, jsonify, render_template
import db
import os
import uuid
from issue import get_issue, parse_issue_url
import datetime
import words

JITSI_INSTANCE = os.environ.get("JITSI_INSTANCE", "https://meet.jit.si/")
GITEA_INSTANCE = "https://codeberg.org"
SUGGESTION_CREATION_BATCH_SIZE = int(os.environ.get("SUGGESTION_CREATION_BATCH_SIZE", 5))

app = Flask(__name__, template_folder="../public", static_url_path="/public", static_folder="../public/")

def get_user():
    username = request.args["codebergUsername"]

    if not username:
        return False

    return db.User.get_or_create(username=username)[0]

@app.route("/")
def index():

    return render_template("index.html")


@app.route("/suggestions")
def suggestions():
    user = get_user()

    if not user:
        return jsonify(False), 403

    searches = db.Search.select()

    create_suggestions(user, searches)

    result = {}

    for suggestion in user.suggestions_received.join(db.Search).join(db.User):
        issue = get_issue(suggestion.search.url, GITEA_INSTANCE)

        result[suggestion.search.url] = {
            "description": issue["body"],
            "title": issue["title"],
            "link": issue["html_url"],
            "comments": issue["comments"],
            "sender": suggestion.search.searcher.username,
            "date" : int(datetime.datetime.strptime(issue["created_at"], "%Y-%m-%dT%H:%M:%SZ").timestamp())
        }

    return jsonify(result)

@app.route("/accepted_suggestions")
def accepted_suggestions():
    user = get_user()

    if not user:
        return jsonify(False), 403

    suggestions = db.Suggestion.select().where(db.Suggestion.accepted_by_receiver).join(db.Search).where(db.Suggestion.search << user.searches)

    result = {}

    for suggestion in suggestions:
        issue = get_issue(suggestion.search.url, GITEA_INSTANCE)

        result[suggestion.search.url] = {
                "title" : issue["title"],
                "receiver": user.username,
                "link" : issue["html_url"]
        }

    return jsonify(result)

def create_suggestions(user, searches):
    app.logger.info(f"Considering {searches.where(db.Search.searcher != user).count()} of {searches.count()} searches for {user}")
    for search in searches.where(db.Search.searcher != user):
        if search.suggestions.where(db.Suggestion.receiver==user).exists():
            app.logger.info(f"Not suggesting {search} to {user}")
            continue
        app.logger.info(f"Suggesting {search} to {user}")

        suggestion = db.Suggestion.create(search=search, receiver=user)


@app.route("/matches")
def matches():
    user = get_user()

    if not user:
        return jsonify(False), 403

    matches = db.Suggestion.select().where(db.Suggestion.accepted_by_receiver).where(db.Suggestion.accepted_by_sender).where((db.Suggestion.receiver == user) | (db.Suggestion.search << user.searches))

    result = {}

    for match in matches:

        result[match.search.url] = {
                "link" : match.search.url,
                "receiver" : match.receiver.username,
                "sender": match.search.searcher.username,
                "meeting" : match.meeting
        }

    return jsonify(result)

@app.route("/accept")
def accept():
    user = get_user()

    if not user:
        return jsonify(False), 403

    url = request.args["issue"]

    suggestion = db.Suggestion.select().where((db.Suggestion.receiver == user) | (db.Suggestion.search << user.searches)).get()

    if user == suggestion.receiver:
        suggestion.accepted_by_receiver = True

    if user == suggestion.search.searcher:
        suggestion.accepted_by_sender = True

    if suggestion.accepted_by_sender and suggestion.accepted_by_receiver:

        suggestion.meeting = generateMeetingURL()

    suggestion.save()

    return jsonify(True)


def generateMeetingURL():
    return JITSI_INSTANCE + words.get_random_words()

@app.route("/reject")
def reject():
    user = get_user()

    if not user:
        return jsonify(False), 403

    url = requests.args["issue"]

    suggesiton = db.Suggestion.select().join(db.Search).get((db.Suggestion.receiver == user) | (db.Suggestion.search << user.searches))

    suggestion.accepted_by_receiver = False if user == suggestion.receiver else suggestion.accepted_by_receiver

    suggestion.accepted_by_sender = False if user == suggestion.search.searcher else suggestion.accepted_by_sender

    suggestion.save()

    return jsonify(True)

@app.route("/searching")
def searching():
    user = get_user()

    if not user:
        return jsonify(False), 403

    url = request.args["issue"]

    search = db.Search.create(
        url=url,
        searcher=user,
        expires=datetime.datetime.now() + datetime.timedelta(days=1)
    )

    return jsonify(True)



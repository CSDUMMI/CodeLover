# CodeLover - match with programmers to work on FOSS Issues
This project is created in part upon the idea of Jan Wildeboer in this [toot](https://social.wildeboer.net/@jwildeboer/109155547897776947).

It's a basic matching algorithm between developers to cooperate on an issue on a [Codeberg](https://codeberg.org) Repository. (Further forges may be added in the future).
